import {AlertType, sendAlert} from "./alert-util";

const APIUrl = 'https://api.bitcraftswe.it/api/';

/**
 * Build the url of the request
 * @param urlParamList {string[]}
 */
function buildUrl(urlParamList) {
    let url = APIUrl;
    urlParamList.forEach(urlParam => url += urlParam + '/');

    return url.substring(0, url.length-1);
}

/**
 * Get a list of all networks
 * @param $http
 * @returns {Promise} the response data | the error
 */
export function getNetworkList($http) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'GET',
            url: buildUrl(['retrieve', 'all'])
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Get a network with the given ID
 * @param $http
 * @param networkID {string} the ID of the network
 * @returns {Promise} the response data | the error
 */
export function getNetwork($http, networkID) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'GET',
            url: buildUrl(['retrieve', networkID])
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Start a network with the given ID
 * @param $http
 * @param networkID {string} the ID of the network
 * @returns {Promise} the response data | the error
 */
export function startNetwork($http, networkID) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'GET',
            url: buildUrl(['start', networkID])
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Stop a network with the given ID
 * @param $http
 * @param networkID {string} the ID of the network
 * @returns {Promise} the response data | the error
 */
export function stopNetwork($http, networkID) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'GET',
            url: buildUrl(['stop', networkID])
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Save a new network with the given json
 * @param $http
 * @param json {JSON} json file containing the definition of the network
 * @returns {Promise} the response data | the error
 */
export function saveNetwork($http, json) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'POST',
            url: buildUrl(['save']),
            headers: {
                'Content-Type': 'application/json'
            },
            data: json
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Delete a network with the given ID
 * @param $http
 * @param networkID {string} the ID of the network
 * @returns {Promise} the response data | the error
 */
export function deleteNetwork($http, networkID) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'GET',
            url: buildUrl(['delete', networkID])
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

export function deleteNetworkAllData($http, network){
    return new Promise((resolve, reject) => {
         let req = {
            method: 'POST',
            url: network.databaseWriteUrl + '/query?pretty=true' + '&db=' + network.databaseWriteName +
                '&q=DROP MEASUREMENT ' + '"' + network.name.replace(/ /g, '_') + '[' + network.id + ']' + '"'
        };

        $http(req).then(
            res => resolve('Table ' + network.name.replace(/ /g, '_') + '[' + network.id + '] dropped successfully'),
            error => reject('Table ' + network.name.replace(/ /g, '_') + '[' + network.id + '] not dropped'));
    });
}

/**
 * Update a network with the given ID
 * @param $http
 * @param networkID {string} the ID of the network
 * @param json {JSON} json file containing the definition of the network
 * @returns {Promise} the response data | the error
 */
export function updateNetwork($http, networkID, json) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'POST',
            url: buildUrl(['update', networkID]),
            headers: {
                'Content-Type': 'application/json'
            },
            data: json
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Get an svg containing the static visualization
 * of the graph of the given network ID
 * @param $http
 * @param networkID {string} the ID of the network
 * @returns {Promise} the response data | the error
 */
export function getStaticGraph($http, networkID) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'GET',
            url: buildUrl(['static-graph', networkID])
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Get an svg containing the dynamic visualization
 * of the graph of the given network ID
 * @param $http
 * @param networkID {string} the ID of the network
 * @returns {Promise} the response data | the error
 */
export function getDynamicGraph($http, networkID) {
    return new Promise((resolve, reject) => {
        let req = {
            method: 'GET',
            url: buildUrl(['dynamic-graph', networkID])
        };

        $http(req).then(res => resolve(res.data), error => reject(error.data));
    });
}

/**
 * Function that creates tables[] and tablesColumnsMap{} of the given database, retrieving data querying the database
 * @param $http
 * @param dbJSON Object{url: string, user: string, password: string, name: string, type: string, grafanaName: string}
 * @returns {Promise<{}>} containing tables and columns of the database in a strange structure
 */
export function getTablesFromDb($http, dbJSON) {
    return new Promise((resolve,reject) => {
        let req = {
            method: 'GET',
            url: dbJSON.url + '/query?u=' + dbJSON.user + '&p=' + dbJSON.password + '&db=' + dbJSON.name + "&q=SHOW FIELD KEYS"
        };

        $http(req).then(
            res => resolve(res),
            error => {
                sendAlert(AlertType.ERROR, 'GnB App Error', error);
                reject(error);
            }
        );
    });
}
