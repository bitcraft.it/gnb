/**
 * @param backendSrv Grafana service
 * returns {Promise} list of databases contained in Grafana parsed with db destination fields
 */
export function getDatabaseList(backendSrv) {
    return new Promise((resolve, reject) => {
        backendSrv.get('api/datasources').then(
            data => {
                let databaseList = [];
                data.forEach(db => {
                    let databaseParsed = {
                        grafanaName: db.name,
                        name: db.database,
                        url: db.url,
                        user: db.user,
                        password: db.password,
                        type: db.type
                    };

                    databaseList.push(databaseParsed);
                });
                resolve(databaseList);
            },
            error => reject(error));
    });
}
