import {sendAlert, AlertType} from "../../../utils/alert-util";
import {getNetwork, getNetworkList, getStaticGraph} from "../../../utils/network-util";
import {Network} from "gnb-network/es6";
import {getDatabaseList} from "../../../utils/grafana-util";
import {generalTabFunctions} from "./general_tab_functions";
import {addNodeTabFunctions} from "./add_node_tab_functions";
import {commonFunctions} from "./common_functions";
import {modifyNodeTabFunctions} from "./modify_node_tab_functions";
import {deleteNodeFunctions} from "./delete_node_tab_function";

class EditNetworkCtrl {

    constructor($scope, $http, $q, $sce, backendSrv, $timeout) {
        $scope.data = {

            waitingPromise: true,
            tab: 0, //index of the selected tab by the user

            databaseList: [],
            databaseSelected: null, //{json} containing usual database information. Initially
                                    // set to null because of an ng-if

            networkList: [], //list of networks given by the API
            networkToFind: null, //{json} network selected by the user in the dropdown menu. Initially
                                    // set to null because of an ng-if

            graphHtml: null, //{string} html containing the graph to display
            networkSelected: null, //{json} network where changes are happening. It could contains wrong data. Initially
                                    // set to null because of an ng-if
            originalNetwork: null, //{Network} original object contained in the API

            originalNode: null,
            nodeSelected: null, //{json} node where changes are happening. It could contains wrong data.Initially
                                // set to null because of an ng-if

            stateSelected: null, //{json} contains user's input data

            numberStates: 2,
            stateList: [],

            predecessorList: [], // Node[]
            successorList: [], // Node[]

            stateListCpt: [], //  [ {index: int, states: [], nodeName: String} ] the list used to show header of the probability table.
            probabilities: [],

            tables: [], //String[] containing tables name
            columns: [], //String[] containing columns name
            tableSelected: null, //{String}
            columnSelected: null, //{String}
            tablesColumnsMap: [], //{table:String, columns: String[]}
            waitingSensor: true,

            modalTrigger: false
        };

        this.loadModelFunctions($scope, $http, $q, $sce, backendSrv, $timeout);
        $scope.loadInitialStatus();
    }

    loadModelFunctions($scope, $http, $q, $sce, backendSrv, $timeout){

        // functions to load initial status of the page

        $scope.open = () => {
            if($scope.data.originalNode) $scope.data.modalTrigger = true;
        };

        $scope.close = () => {
            $scope.data.modalTrigger = false;
        };

        /**
         * Function that loads initial values of the page such as the databaseList contained in Grafana data sources
         * and collect networks list from API.
         * It stops $scope.data.waitingPromise
         */
        $scope.loadInitialStatus = () => {
            $q.all(Array.of(
                getDatabaseList(backendSrv).then(
                    list => $scope.data.databaseList = list,
                    error => sendAlert(AlertType.ERROR, 'GnB App Error', error)
                ),
                getNetworkList($http).then(
                    data => $scope.data.networkList = data,
                    error => sendAlert(AlertType.ERROR, 'GnB App Error', error)
                )
            )).then(() => {
                $scope.data.waitingPromise = false;
                $scope.loadTabFunctions();
            });
        };

        $scope.refreshGraph = () => {
                getStaticGraph($http, $scope.data.originalNetwork.id).then(
                    data => {
                        $scope.data.graphHtml = $sce.trustAsHtml(data);
                        $scope.refreshPage().then(() => {
                            let element = document.getElementById($scope.data.originalNetwork.id + '_STATIC');
                            let bbox = element.getBBox();
                            element.setAttribute('width', (bbox.width + bbox.x) + "px");
                            element.setAttribute('height', (bbox.height + bbox.y) + "px");
                            $scope.refreshPage();
                        });

                    },
                    error => sendAlert(AlertType.ERROR, 'GnB App Error', error)
                );
        };

        /**
         * Function that fills values of $scope.data.networkSelected, $scope.data.originalNetwork.
         */
        $scope.setNetworkSelected = () => {
            getNetwork($http, $scope.data.networkToFind.id).then(network => {
                $scope.data.networkSelected = network;
                $scope.data.originalNetwork = Network.fromJSON(network);
                $scope.setTab(0);
                $scope.refreshGraph();
            });
        };

        /**
         * Utility function that loads functions of each tab
         */
        $scope.loadTabFunctions = () => {
            generalTabFunctions($scope, $http);
            addNodeTabFunctions($scope, $http);
            modifyNodeTabFunctions($scope,$http);
            deleteNodeFunctions($scope,$http, $q);
            commonFunctions($scope,$http, $timeout);
        };

        /**
         * Function that creates or removes state objects from $scope.data.stateList.
         * The functions listens to the user input and add or remove objects as consequence of that input
         */
        $scope.populateStateList = () => {
            let limit = $scope.data.stateList.length;

            if ($scope.data.numberStates>=limit){
                while(limit < $scope.data.numberStates){
                    $scope.data.stateList.push({});
                    limit++;
                }
            }
            else{
                while(limit > $scope.data.numberStates){
                    $scope.data.stateList.pop();
                    limit--;
                }
            }
        };

        /**
         * Function that creates or removes entries in $scope.data.probabilities.
         * The functions listens to the user input and add or remove objects as consequence of that input.
         * It push() or pop() an array inside $scope.data.probabilities if the user adds or removes a state.
         * It push() or pop() an entry in all the arrays inside $scope.data.probabilities if the user adds or removes a parent
         * from predecessorList
         */
        $scope.populateProbabilities = () => {

            let limit = $scope.data.probabilities.length;

            if ($scope.data.numberStates !== null) {
                if ($scope.data.numberStates >= limit) {
                    while ($scope.data.numberStates > limit){
                        $scope.data.probabilities.push([]);
                        limit++;
                    }
                }
                else {
                    while(limit > $scope.data.numberStates){
                        $scope.data.probabilities.pop();
                        limit--;
                    }
                }

                let maxNumberStates = $scope.getStateList($scope.data.stateListCpt.length-1).length; //number of states on the last line of the probability table

                let numberProbs = 0;
                if (limit>0)
                    numberProbs = $scope.data.probabilities[0].length;


                if (limit>0 && maxNumberStates >= numberProbs) {
                    while (maxNumberStates > numberProbs) {
                        $scope.data.probabilities.forEach((list) => {
                            list.push(null);
                        });
                        numberProbs++;
                    }
                }
                else {
                    while(numberProbs > maxNumberStates) {
                        $scope.data.probabilities.forEach((list) => {
                            list.pop();
                        });
                        numberProbs--;
                    }
                }
            }
            else{
                $scope.data.probabilities = [];
            }
        };
    };
}

EditNetworkCtrl.templateUrl = 'public/plugins/gnb/component/pages/edit_network/edit_network.html';

export {EditNetworkCtrl};
