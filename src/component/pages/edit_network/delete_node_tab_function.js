import {updateNetwork} from "../../../utils/network-util";
import {AlertType, sendAlert} from "../../../utils/alert-util";
import {Network} from "gnb-network/es6/src/network";

export function deleteNodeFunctions($scope, $http) {

    /**
     * Delete the node selected
     */
    $scope.deleteNode = () => {
        let tempNetwork = Network.fromJSON(JSON.parse(JSON.stringify($scope.data.originalNetwork)));

        try{
            $scope.data.originalNetwork.nodes.forEach((node) => {
                if (node.parentList.includes($scope.data.originalNode.id)) {
                    node.parentList = node.parentList.filter((parent) => parent !== $scope.data.originalNode.id);
                    node.divideCpt($scope.data.originalNode);
                }
            });
            $scope.data.originalNetwork.removeNodeFromObject($scope.data.originalNode);
        }catch(error){
            sendAlert(AlertType.WARNING, 'Remove node error', error)
        }

        updateNetwork($http, $scope.data.originalNetwork.id, $scope.data.originalNetwork.getJSON()).then(
            data => {
                sendAlert(AlertType.SUCCESS, 'Network ' + $scope.data.originalNetwork.name, data);
                $scope.refreshGraph();
            },
            error => {
                sendAlert(AlertType.WARNING, 'Network ' + $scope.data.originalNetwork.name, error);
                $scope.data.originalNetwork = Network.fromJSON(JSON.parse(JSON.stringify(tempNetwork)));
                $scope.refreshPage();
            }
        );
    };
}
