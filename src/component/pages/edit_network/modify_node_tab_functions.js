import {Node} from "gnb-network/es6/src/node";
import {StateBuilder} from "gnb-network/es6/src/builder/StateBuilder";
import {AlertType, sendAlert} from "../../../utils/alert-util";
import {updateNetwork} from "../../../utils/network-util";
import {Network} from "gnb-network/es6/src/network";

export function modifyNodeTabFunctions($scope, $http) {

    /**
     *
     */
    $scope.setNodeSelected = () => {
        $scope.data.nodeSelected = JSON.parse(JSON.stringify($scope.data.originalNode));
    };

    /**
     *
     */
    $scope.setNodePredecessors = () => {
        let list = [];

        $scope.data.originalNode.parentList.forEach((nodeID) => {
            let newPred = $scope.data.originalNetwork.nodes.find((node) => node.id === nodeID);
            list.push(newPred);
        });

        $scope.data.predecessorList = list;
    };

    /**
     *
     */
    $scope.setNodeSuccessors = () => {
        let list = [];

        $scope.data.originalNetwork.nodes.forEach((node) => {
            if (node.parentList.includes($scope.data.originalNode.id))
                list.push(node);
        });

        $scope.data.successorList = list;
    };

    $scope.filterNode = (node) => {
        return node.id !== $scope.data.originalNode.id;
    };

    /**
     *
     */
    $scope.setExistingSensorData = () => {

        if ($scope.data.originalNode.hasSensor()){
            let selectedDatabase = $scope.data.databaseList.find(db => db.url === $scope.data.originalNode.sensor.databaseSensorUrl &&
                db.name === $scope.data.originalNode.sensor.databaseSensorName);

            if (selectedDatabase){
                $scope.data.databaseSelected = selectedDatabase;

                $scope.setTableList().then(() => {
                    $scope.data.tableSelected = $scope.data.originalNode.sensor.databaseSensorTable;
                    $scope.setColumnList();
                    $scope.data.columnSelected = $scope.data.originalNode.sensor.databaseSensorColumn;
                    $scope.refreshPage();
                });
            }
            else{
                $scope.data.databaseSelected = null;
                $scope.data.tablesColumnsMap = [];
                $scope.data.tables = [];
                $scope.data.columns = [];
            }
        }
        else{
            $scope.data.databaseSelected = null;
            $scope.data.tablesColumnsMap = [];
            $scope.data.tables = [];
            $scope.data.columns = [];
        }
    };

    /**
     *
     */
    $scope.setExistingStates = () => {
        $scope.data.numberStates = $scope.data.originalNode.stateList.length;
        $scope.data.stateList = [];
        $scope.populateStateList();

        let index = 0;
        $scope.data.originalNode.stateList.forEach((state) => {
            $scope.data.stateList[index].name = state.name;

            let split = state.trigger.split('%v').filter(val => val !== "");

            if (split.length === 1) {
                if (split[0].includes("<=")){
                    $scope.data.stateList[index].minInterval = parseInt(split[0].replace("<=", ""));
                }
                else {
                    $scope.data.stateList[index].maxInterval = parseInt(split[0].replace("<", ""));
                }
            }
            else {
                $scope.data.stateList[index].minInterval = parseInt(split[0].replace("<=", ""));
                $scope.data.stateList[index].maxInterval = parseInt(split[1].replace("<", ""));
            }
            index++;
        });
    };

    /**
     *
     */
    $scope.setExistingProbabilities = () => {
        $scope.createCpt();
        $scope.populateProbabilities();

        let size = $scope.data.probabilities[0].length;
        for (let i = 0; i<size; i++) {

        }

        let row = 0;
        let column = 0;
        $scope.data.nodeSelected.cptList.forEach((cpt) => {
            row = 0;
            cpt.forEach((entry) => {
                $scope.data.probabilities[row][column] = parseFloat(entry);
                row++;
            });
            column++;
        })
    };

    /**
     *
     */
    $scope.setExistingData = () => {
        $scope.setNodeSelected();
        $scope.setNodePredecessors();
        $scope.setNodeSuccessors();
        $scope.setExistingStates();
        $scope.setExistingSensorData();
        $scope.setExistingProbabilities();
        $scope.refreshPage()
    };

    /**
     * Update a node edited by user using Builder pattern
     */
    $scope.updateNode = () => {

        let stateBuilder = new StateBuilder();
        let createdState = {};
        let stateList = [];

        let createdSensor = {};

        let tempNetwork = Network.fromJSON($scope.data.originalNetwork.getJSON());

        //building parentList using predecessors chosen
        let parentList = [];
        $scope.data.predecessorList.forEach((node) => {
            parentList.push(node.id);
        });

        try {
            $scope.data.originalNode.setName($scope.data.nodeSelected.name);

            $scope.data.originalNode.setParentList(parentList);

            $scope.data.stateList.forEach((state) => {
                createdState = $scope.createState(state, stateBuilder);
                stateList.push(createdState);
            });

            $scope.data.originalNode.setStateList(stateList);

            if ($scope.data.databaseSelected) {
                createdSensor = $scope.createSensor($scope.data.databaseSelected,$scope.data.tableSelected, $scope.data.columnSelected);
                $scope.data.originalNode.setSensor(createdSensor);
            }

            $scope.data.originalNode.setCptList($scope.getCptTable($scope.data.probabilities));

            $scope.data.successorList.forEach((successor) => {
                if (!successor.parentList.includes($scope.data.originalNode.id)) {
                    successor.addParent($scope.data.originalNode.id);
                    successor.multiplyCpt($scope.data.originalNode);
                }
            });
            $scope.data.originalNetwork.nodes.forEach((node) => {
               if (node.parentList.includes($scope.data.originalNode.id) && !$scope.data.successorList.includes(node)) {
                   node.divideCpt($scope.data.originalNode);
                   node.parentList = node.parentList.filter((parent) => parent !== $scope.data.originalNode.id);
               }
            });

            if($scope.data.originalNetwork.hasCycle()) {
                $scope.data.originalNetwork = tempNetwork;
                $scope.setTab(1);
                throw "The network contains a cycle.";
            }

            updateNetwork($http, $scope.data.originalNetwork.id, $scope.data.originalNetwork.getJSON()).then(
                data => {
                    sendAlert(AlertType.SUCCESS, 'Network ' + $scope.data.originalNetwork.name, data);
                    $scope.refreshGraph();
                },
                error => {
                    sendAlert(AlertType.WARNING, 'Network ' + $scope.data.originalNetwork.name, error);
                    $scope.refreshPage();
                }
            );
        }
        catch (error) {
            sendAlert(AlertType.ERROR, 'Error adding the node', error);
        }
    };
}
