import {getTablesFromDb} from "../../../utils/network-util";

export function commonFunctions($scope, $http, $timeout) {
    /**
     * Function invoked each time that the tab is changed.
     * It clears fields to keep data independent for each tab
     * @param val {int}
     */
    $scope.setTab = (val) => {
        $scope.data.tab = val;
        $scope.data.networkSelected = $scope.data.originalNetwork.getJSON();
        $scope.reset();

        if (val === 0) {
            $scope.data.databaseSelected = $scope.data.databaseList.find(db => db.url === $scope.data.originalNetwork.databaseWriteUrl
                && db.name === $scope.data.originalNetwork.databaseWriteName);
        }

        if (val === 1) {
            $scope.data.nodeSelected = null;
            $scope.data.originalNode = null;
        }

        if (val === 2) {
            $scope.data.nodeSelected = null;
            $scope.data.numberStates = 2;
            $scope.data.stateList = [];
            $scope.data.stateListCpt = [];
            $scope.data.probabilities = [];
            $scope.data.predecessorList = [];
            $scope.data.successorList = [];
            $scope.populateStateList();
            $scope.populateProbabilities();
        }

        if (val === 3) {
            $scope.data.originalNode = null;
        }

    };

    /**
     * utility function that clear data from $scope when tab is changed by the user.
     */
    $scope.reset = () => {
        $scope.data.tables = [];
        $scope.data.columns = [];
        $scope.data.tablesColumnsMap = [];
        $scope.data.databaseSelected = null;
        $scope.data.waitingSensor = true;
    };


    /**
     * Utility function that returns an empty array of length = {number}
     * @param number, the desired length of the array
     * @returns {null[]}
     */
    $scope.range = (number) => {
        if (!number || parseInt(number)<0)
            return Array(0);
        else
            return new Array(parseInt(number));
    };



    /**
     * Function that sets $scope.data.tables and $scope.data.tablesColumnsMap with
     * data of the chosen database
     */
    $scope.setTableList = () => {
        $scope.data.columns = [];
        $scope.data.tables = [];
        $scope.data.tableSelected = null;
        $scope.data.columnSelected = null;
        return new Promise((resolve,reject) => {
            $scope.data.waitingSensor = true;
            $scope.getTableList($scope.data.databaseSelected).then( (tableAndMapList) => {
                $scope.data.tables = tableAndMapList.tables;

                $scope.data.tablesColumnsMap = tableAndMapList.tablesColumnsMap;
                $scope.data.waitingSensor = false;
                $scope.refreshPage();
                resolve();
            }, error => {
                reject("Error reaching the database");
            });
        });
    };

    /**
     * Function that creates tables[] and tablesColumnsMap{} of the given database, retrieving data querying the database
     * @param dbJSON Object{url: string, user: string, password: string, name: string, type: string, grafanaName: string}
     * @returns {Promise<{tables: String[], tablesColumnsMap:{ table:String, columns: String[]}[] }>}
     */
    $scope.getTableList = (dbJSON) =>{

        let objectToReturn = {
            tables: [],
            tablesColumnsMap: []
        };

        return new Promise((resolve,reject) => {
            getTablesFromDb($http, dbJSON).then( (rawJSON) => {
                if (rawJSON.data.results[0].series) {
                    rawJSON.data.results[0].series.forEach((table) => {
                        let tableColumnsEntry = {
                            table: table.name,
                            columns: []
                        };

                        table.values.forEach( (col) => {
                            tableColumnsEntry.columns.push(col[0]);
                        });

                        objectToReturn.tables.push(table.name);
                        objectToReturn.tablesColumnsMap.push(tableColumnsEntry);
                    });
                }

                resolve(objectToReturn);
            });
        });
    };


    /**
     * Function that puts columns of the chosen table into $scope.data.columns
     */
    $scope.setColumnList = () => {
        if ($scope.data.tablesColumnsMap.length>0) {
            $scope.data.columns = $scope.data.tablesColumnsMap.find((entry) => entry.table === $scope.data.tableSelected).columns;
        }
    };

    /**
     *
     * @returns {Promise<any>}
     */
    $scope.refreshPage = () => {
        return new Promise((resolve => {
            $timeout(() => {},1).then(() => {
                resolve();
            });
        }))
    }
}
