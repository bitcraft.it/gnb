import {AlertType, sendAlert} from "../../../utils/alert-util";
import {updateNetwork} from "../../../utils/network-util";

export function generalTabFunctions($scope, $http) {
    $scope.updateNetworkGeneralData = () => {
        //set desired user's inputs in originalNetwork object
        try {
            $scope.data.originalNetwork.setName($scope.data.networkSelected.name);
            $scope.data.originalNetwork.setRefreshTime($scope.data.networkSelected.refreshTime);
            let database = $scope.data.databaseSelected;
            $scope.data.originalNetwork.setDatabaseFromData(database.type,database.name,database.url,database.user,database.password);

            updateNetwork($http, $scope.data.originalNetwork.id, $scope.data.originalNetwork.getJSON()).then(
                data => sendAlert(AlertType.SUCCESS, 'Network ' + $scope.data.originalNetwork.name, data),
                error => sendAlert(AlertType.WARNING, 'Network ' + $scope.data.originalNetwork.name, error)
            );

        } catch (error) {
            sendAlert(AlertType.ERROR, 'Error modifying network data', error);
        }


    };

};
