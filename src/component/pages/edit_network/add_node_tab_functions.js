import {StateBuilder} from "gnb-network/es6/src/builder/StateBuilder";
import {NodeBuilder} from "gnb-network/es6/src/builder/NodeBuilder";
import {SensorBuilder} from "gnb-network/es6/src/builder/SensorBuilder";
import {AlertType, sendAlert} from "../../../utils/alert-util";
import {updateNetwork} from "../../../utils/network-util";
import {generateUID} from "gnb-network/es6/src/util/uid-util";
import {Network} from "gnb-network/es6/src/network";
import {Node} from "gnb-network/es6/src/node";

export function addNodeTabFunctions($scope, $http) {

    /**
     * Creates a State object from given parameters
     * @param state Object{name: {String}, minInterval: {Int}, maxInterval: {Int}}
     * @param stateBuilder {StateBuilder}
     * @returns {State}
     * @throws {string} error message
     */
    $scope.createState = (state, stateBuilder) => {

        if (angular.equals(state, {}))
            throw "One of the state added is not defined";
        stateBuilder.withName(state.name);
        stateBuilder.withMinTrigger(state.minInterval);
        stateBuilder.withMaxTrigger(state.maxInterval);

        return stateBuilder.build();
    };

    /**
     * Creates a Sensor object from the given parameters
     * @param database Object{name: {String}, url: {String}, user: {String}, password: {String}, type: {String}}
     * @param table {String}
     * @param column {String}
     * @returns {Sensor}
     */
    $scope.createSensor = (database,table, column) => {
        let sensorBuilder = new SensorBuilder();

        sensorBuilder.withdatabaseSensorName(database.name);
        sensorBuilder.withdatabaseSensorUrl(database.url);
        sensorBuilder.withdatabaseSensorUser(database.user);
        sensorBuilder.withdatabaseSensorPassword(database.password);
        sensorBuilder.withDatabaseSensorType(database.type);
        sensorBuilder.withdatabaseSensorTable(table);
        sensorBuilder.withdatabaseSensorColumn(column);

        return sensorBuilder.build();
    };

    /**
     * Function that returns an Array of arrays, containing probabilities in the form used by the Network
     * library.
     * @param probabilities: should pass $scope.data.probabilities
     * @returns {Array} in the form wanted by Network library
     */
    $scope.getCptTable = (probabilities) => {
        let cptTable = [];
        let size = probabilities[0].length;

        for (let i = 0; i<size; i++){
            let entry = [];

            probabilities.forEach((list) => {
                entry.push(list[i]);
            });
            cptTable.push(entry);
        }
        return cptTable;

    };


    /**
     * Add a node created by user using Builder pattern
     */
    $scope.addNode = () => {
        let nodeBuilder = new NodeBuilder();
        let createdNode = {};

        let stateBuilder = new StateBuilder();
        let createdState = {};
        let stateList = [];

        let createdSensor = {};

        let tempNetwork = Network.fromJSON($scope.data.originalNetwork.getJSON());

        //building parentList using predecessors chosen
        let parentList = [];
        $scope.data.predecessorList.forEach((node) => {
            parentList.push(node.id);
        });


        try {
            if (!$scope.data.nodeSelected)
                throw "The node's name must be set";

            nodeBuilder.withName($scope.data.nodeSelected.name);
            nodeBuilder.withParents(parentList);

            //create states and add them to local stateList.
            $scope.data.stateList.forEach((state) => {
                createdState = $scope.createState(state, stateBuilder);
                stateList.push(createdState);
            });

            nodeBuilder.withStates(stateList);

            if ($scope.data.databaseSelected)
                createdSensor = $scope.createSensor($scope.data.databaseSelected,$scope.data.tableSelected, $scope.data.columnSelected);

            nodeBuilder.withSensor(createdSensor);

            nodeBuilder.withCpt($scope.getCptTable($scope.data.probabilities));

            nodeBuilder.withId(generateUID());

            createdNode = nodeBuilder.build();
            $scope.data.originalNetwork.addNode(createdNode);

            $scope.data.successorList.forEach((successor) => {
                successor.addParent(createdNode.id);
                successor.multiplyCpt(createdNode);
            });

            if($scope.data.originalNetwork.hasCycle()) {
                $scope.data.originalNetwork = tempNetwork;
                $scope.setTab(2);
                throw "The network contains a cycle.";
            }

            updateNetwork($http, $scope.data.originalNetwork.id, $scope.data.originalNetwork.getJSON()).then(
                data => {
                    sendAlert(AlertType.SUCCESS, 'Network ' + $scope.data.originalNetwork.name, data);
                    $scope.refreshGraph();
                    $scope.setTab(2);
                },
                error => {
                    sendAlert(AlertType.WARNING, 'Network ' + $scope.data.originalNetwork.name, error);
                    $scope.refreshPage();
                }
            );
        } catch (error) {
            sendAlert(AlertType.ERROR, 'Error adding the node', error);
        }
    };


    /**
     * Function that create $scope.data.stateListCpt, the list used to show header of the probability table.
     * $scope.data.stateListCpt = [ {index: int, states: [], nodeName: String} ]
     */
    $scope.createCpt = () => {
        let stateListShow = [];
        let index = 0;
        $scope.data.predecessorList.forEach((node) => {
            let entryList = {
                index: index,
                states: [],
                nodeName: node.name
            };

            if (index!==0){
                let size = stateListShow.find((entry) => entry.index === index-1).states.length;
                for (let i = 0; i<size; i++){
                    node.stateList.forEach((state) => {
                        entryList.states.push(state);
                    });
                }
            }
            else{
                node.stateList.forEach((state) => {
                    entryList.states.push(state);
                });
            }

            stateListShow.push(entryList);
            index++;
        });

        $scope.data.stateListCpt = stateListShow;
    };

    /**
     * Function that return the state list of the node in the nth-row visualized by the user
     * in the probability table section. If the row isn't found, return [null]
     * @param row
     * @returns {Array|State[]|null|*[]}
     */
    $scope.getStateList = (row) => {
        let entry = $scope.data.stateListCpt.find((entry) => entry.index === row);
        if (entry !== undefined)
            return entry.states;
        return [null];
    };
}
