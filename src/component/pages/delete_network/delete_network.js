import {sendAlert, AlertType} from "../../../utils/alert-util";
import {deleteNetwork, deleteNetworkAllData, saveNetwork, getNetwork, getNetworkList} from '../../../utils/network-util'
import {Network} from "gnb-network/es6/src/network";
class DeleteNetworkCtrl {

    constructor($scope, $http, $q, $timeout) {
        $scope.data = {
            networkList: [],
            networkToFind: null,
            networkSelected: null,
            waitingPromise: true,
            checked: false,
            modalTrigger: false,
            textMessageHTML: ""
        };

        $q.all(Array.of(
            getNetworkList($http).then(
                data => $scope.data.networkList = data,
                error => sendAlert(AlertType.ERROR, 'GnB App Error', error)
            )
        )).then(() => {
            $scope.data.waitingPromise = false;
            this.loadModelFunctions($scope, $http, $timeout);
        });
    }

    loadModelFunctions($scope, $http, $timeout) {
        $scope.refreshPage = () => {
            $timeout(() => {},1);
        };

        /**
         * Open and set the text of the pop-up
         */
        $scope.open = () => {
            if($scope.data.networkSelected) {
                $scope.data.modalTrigger = true;
                $scope.data.textMessageHTML = 'Are you sure you want to delete <b>'
                    + $scope.data.networkSelected.name + '[' + $scope.data.networkSelected.id + '] </b>';
                if($scope.data.checked){
                    $scope.data.textMessageHTML += 'and all data contained in database <b>'
                        + $scope.data.networkSelected.databaseWriteName + '</b>';
                }
            }
        };

        /**
         * Close the pop-up
         */
        $scope.close = () => {
            $scope.data.modalTrigger = false;
        };

        /**
         * When a network is selected, obtain that network object
         */
        $scope.setNetworkSelected = () => {
            if($scope.data.networkToFind){
                getNetwork($http, $scope.data.networkToFind.id).then(network => {
                    $scope.data.networkSelected = network;
                    $scope.refreshPage();
                });
            }
        };

        /**
         * Delete the network selected
         */
        $scope.delete = () => {
            deleteNetwork($http, $scope.data.networkSelected.id).then(
                data => {
                    sendAlert(AlertType.SUCCESS, 'Network ' + $scope.data.networkSelected.name, data);

                    if($scope.data.checked){
                        deleteNetworkAllData($http, $scope.data.networkSelected).then(
                            data => {
                                sendAlert(AlertType.SUCCESS, 'Database ' + $scope.data.networkSelected.databaseWriteName, data);
                                $scope.data.networkList = $scope.data.networkList.filter((entry) => entry.id !== $scope.data.networkSelected.id);
                                $scope.data.networkSelected = null;
                                $scope.networkToFind = null;
                                $scope.refreshPage();
                            },
                            error => {
                                sendAlert(AlertType.WARNING, 'Database ' + $scope.data.networkSelected.databaseWriteName, error);
                                saveNetwork($http, $scope.data.networkSelected).then(
                                    () => sendAlert(AlertType.SUCCESS, 'Network ' + $scope.data.networkSelected.name, 'Network restored successfully'),
                                    () => sendAlert(AlertType.WARNING, 'Network ' + $scope.data.networkSelected.name, 'Network not restored successfully')
                                );
                            }
                        );
                    } else{
                        $scope.data.networkList = $scope.data.networkList.filter((entry) => entry.id !== $scope.data.networkSelected.id);
                        $scope.data.networkSelected = null;
                        $scope.networkToFind = null;
                        $scope.refreshPage();
                    }
                },
                error => sendAlert(AlertType.WARNING, 'Network ' + $scope.data.networkSelected.name, error)
            );
        }
    }

}

DeleteNetworkCtrl.templateUrl = 'public/plugins/gnb/component/pages/delete_network/delete_network.html';

export {DeleteNetworkCtrl};
