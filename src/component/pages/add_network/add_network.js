import {sendAlert, AlertType} from "../../../utils/alert-util";
import {Network} from "gnb-network/es6";
import {saveNetwork} from '../../../utils/network-util'

class AddNetworkCtrl {

    constructor($scope, $http, backendSrv, $q, $interval) {
        $scope.data = {
            fileNameSelected: 'Select file...',
            promise: null
        };

        this.loadModelFunctions($scope, $http, $interval);
    }

    loadModelFunctions($scope, $http, $interval) {
        $scope.loadNetwork = () => {
                let file = document.getElementById('net_file').files[0];

                if(file.type === "application/json"){
                    let fileReader = new FileReader();

                    fileReader.onload = event => {
                        try {
                            let contents = JSON.parse(event.target.result);
                            let network = Network.fromJSON(contents);

                            saveNetwork($http, network.getJSON()).then(
                                data => sendAlert(AlertType.SUCCESS, 'Network ' + contents.name, data),
                                error => sendAlert(AlertType.WARNING, 'Network ' + contents.name, error)
                            );
                        } catch (error) {
                            sendAlert(AlertType.ERROR, 'Error uploading the network', error);
                        }
                    };

                    fileReader.readAsText(file);
                    $interval.cancel($scope.data.promise);
                }else{
                    sendAlert(AlertType.ERROR, "File type error", "The file uploaded is not JSON");
                }

        };

        $scope.setNameFile = () => {
            let element = document.getElementById("net_file");

            $scope.data.promise = $interval(() => {
                if (element.files[0]) {
                    $scope.data.fileNameSelected = element.files[0].name;
                }
            },500);
        };
    }

}

AddNetworkCtrl.templateUrl = 'public/plugins/gnb/component/pages/add_network/add_network.html';

export {AddNetworkCtrl};
