import {getDatabaseList} from "../../../utils/grafana-util";
import {Network} from "gnb-network/es6";
import {generateUID} from "gnb-network/es6/src/util/uid-util";
import {NetworkBuilder} from "gnb-network/es6/src/builder/NetworkBuilder";
import {AlertType, sendAlert} from "../../../utils/alert-util";
import {saveNetwork} from "../../../utils/network-util";

class CreateNetworkCtrl {

    constructor($scope, $http, backendSrv, $q) {
        $scope.data = {
            networkName: null,
            networkRefreshTime: null,
            databaseList: [],
            databaseSelected: null,
            waitingPromise: true
        };

        $q.all(Array.of(
            getDatabaseList(backendSrv).then(
                list => {$scope.data.databaseList = list;},
                error => sendAlert(AlertType.ERROR, 'GnB App Error', error)
            )
        )).then(() => {
            $scope.data.waitingPromise = false;
            this.loadModelFunctions($scope, $http);
        });
    }

    loadModelFunctions($scope, $http) {
        /**
         * Function that creates a new network with parameters chosen by the user.
         * It calls the method saveNetwork() that saves the network into the API
         */
        $scope.createNetwork = () => {
            let networkBuilder = new NetworkBuilder();
            let createdNetwork = {};

            //build the network with inserted parameters
            try {
                networkBuilder.withName($scope.data.networkName);
                networkBuilder.withRefreshTime($scope.data.networkRefreshTime);

                networkBuilder.withDatabaseWriteName($scope.data.databaseSelected.name);
                networkBuilder.withDatabaseWriteType($scope.data.databaseSelected.type);
                networkBuilder.withDatabaseWriteUrl($scope.data.databaseSelected.url);
                networkBuilder.withDatabaseWriteUser($scope.data.databaseSelected.user);
                networkBuilder.withDatabaseWritePassword($scope.data.databaseSelected.password);

                networkBuilder.withId(generateUID());
                networkBuilder.withNodes([]);

                createdNetwork = networkBuilder.build();

                //save the network in the API
                saveNetwork($http, createdNetwork).then(
                    data => sendAlert(AlertType.SUCCESS, 'Network ' + createdNetwork.name, data),
                    error => sendAlert(AlertType.WARNING, 'Network ' + createdNetwork.name, error)
                );
            } catch (error) {
                sendAlert(AlertType.ERROR, 'Error during network creation', error)
            }
        };
    }

}

CreateNetworkCtrl.templateUrl = 'public/plugins/gnb/component/pages/create_network/create_network.html';

export {CreateNetworkCtrl};
