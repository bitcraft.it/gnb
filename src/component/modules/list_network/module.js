import {loadPluginCss, PanelCtrl} from 'grafana/app/plugins/sdk';
import {sendAlert, AlertType} from "../../../utils/alert-util";
import {getNetwork, getNetworkList, startNetwork, stopNetwork} from '../../../utils/network-util'
import { saveAs } from 'file-saver';

loadPluginCss({
    dark: 'plugins/gnb/css/gnb.dark.css'
});

class ListNetworkCtrl extends PanelCtrl {

    constructor($scope, $injector, $http, $q) {
        super($scope, $injector);

        $scope.data = {
            networkList: [],
            waitingPromise: true
        };

        $q.all(Array.of(
            getNetworkList($http).then(
                data => {$scope.data.networkList = data;},
                error => sendAlert(AlertType.ERROR, 'GnB App Error', error)
            )
        )).then(() => {
            $scope.data.waitingPromise = false;
            this.loadModelFunctions($scope, $http);
        });
    }

    loadModelFunctions($scope, $http) {
        /**
         * Function that starts a specific network.
         * It calls the function startNetwork() that makes an API request.
         * @param network {Network} the network to start
         */
        $scope.startNet = (network) => {
            startNetwork($http, network.id).then(
                data => {
                    sendAlert(AlertType.SUCCESS, 'Network ' + network.name, data);
                    network.active = true;
                    $scope.$apply();
                },
                error => sendAlert(AlertType.WARNING, 'Network ' + network.name, error)
            );
        };

        /**
         * Function that stops a specific network.
         * It calls the function stopNetwork() that makes an API request.
         * @param network {Network} the network to stop
         */
        $scope.stopNet = (network) => {
            stopNetwork($http, network.id).then(
                data => {
                    sendAlert(AlertType.SUCCESS, 'Network ' + network.name, data);
                    network.active = false;
                    $scope.$apply();
                },
                error => sendAlert(AlertType.WARNING, 'Network ' + network.name, error)
            );
        };

        /**
         * Function that downloads a file containing the JSON definition of a specific network.
         * It calls the function getNetwork() that makes an API request.
         * @param network {Network} the network to download
         */
        $scope.downloadJSON = (network) => {
            getNetwork($http, network.id).then(
                data => {
                    let fileName = network.name+'.json';

                    let fileToSave = new Blob([JSON.stringify(data)], {
                        type: 'application/json',
                        name: fileName
                    });
                    saveAs(fileToSave, fileName);
                },
                error => sendAlert(AlertType.WARNING, 'Network ' + network.name, error)
            )
        };
    }

}

ListNetworkCtrl.templateUrl = 'component/modules/list_network/list_network.html';

export {ListNetworkCtrl as PanelCtrl};
