import {loadPluginCss, PanelCtrl} from 'grafana/app/plugins/sdk';
import {sendAlert, AlertType} from "../../../utils/alert-util";
import {getStaticGraph, getDynamicGraph, getNetworkList} from '../../../utils/network-util'

loadPluginCss({
    dark: 'plugins/gnb/css/gnb.dark.css'
});

class DisplayNetworkCtrl extends PanelCtrl {

    constructor($scope, $injector, $http, $q, $sce) {
        super($scope, $injector);

        $scope.data = {
            graphHtml: null,
            error: false,
            errorMessage: null,
            existingGraph: false
        };
        this.networkList = [];
        this.waitingPromise = true;

        $q.all(Array.of(
            getNetworkList($http).then(
                data => {
                    data.forEach(entry => delete entry.active);
                    this.networkList = data;
                },
                error => sendAlert(AlertType.ERROR, 'GnB App Error', error)
            )
        )).then(() => {
            this.waitingPromise = false;
            this.loadModelFunctions($scope, $http, $sce);
            $scope.checkGraphType();
        });
    }

    loadModelFunctions($scope, $http, $sce) {
        /**
         * Function that refreshes the graph, checking the display type.
         * Based on the display type, it calls the functions
         * getStaticGraph() or getDynamicGraph() that make an API request.
         */
        $scope.refreshGraph = () => {
            let getGraph = this.panel.displayType === "STATIC" ?
                getStaticGraph($http, this.panel.networkSelected.id) :
                getDynamicGraph($http, this.panel.networkSelected.id);

            getGraph.then(
                data => {
                    $scope.data.error = false;
                    $scope.data.graphHtml = $sce.trustAsHtml(data);
                    $scope.$apply();

                    let element = document.getElementById(this.panel.networkSelected.id + '_' + this.panel.displayType);
                    let bbox = element.getBBox();
                    element.setAttribute('width', (bbox.width + bbox.x) + "px");
                    element.setAttribute('height', (bbox.height + bbox.y) + "px");
                },
                error => {
                    $scope.data.error = true;
                    $scope.data.errorMessage = error;
                    $scope.$apply();
                }
            )
        };

        /**
         * Function that checks the display type.
         * Based on the display type, it adds or removes a listener
         * for each page refresh and eventually refreshes the graph.
         */
        $scope.checkGraphType = () => {
            //check if already exist the same graph in another panel
            if(this.panel.networkSelected && document.getElementById(this.panel.networkSelected.id + '_' + this.panel.displayType)){
                $scope.data.existingGraph = true;
                $scope.data.graphHtml = "";
                this.panel.title = "Display Graph - [" + this.panel.displayType + "] " + this.panel.networkSelected.name;
            }
            else {
                $scope.data.existingGraph = false;
                if (this.panel.hasOwnProperty('networkSelected') && this.panel.networkSelected !== {} &&
                    this.panel.hasOwnProperty('displayType') && this.panel.displayType !== "") {
                    this.panel.title = "Display Graph - [" + this.panel.displayType + "] " + this.panel.networkSelected.name;

                    //Add or remove the listener on panel refresh
                    if(this.panel.displayType === "STATIC"){
                        $scope.ctrl.events.removeAllListeners();
                        $scope.refreshGraph();
                    }else{
                        $scope.refreshGraph();
                        $scope.ctrl.events.on('refresh', $scope.refreshGraph.bind(this));
                    }
                }
            }

        };
    }

    /**
     * Function that sets the display type of the graph.
     * The info is also saved on the panel's JSON.
     */
    setDisplayType() {
        this.$scope.checkGraphType();
    }

    /**
     * Function that sets the network to display.
     * The info is also saved on the panel's JSON.
     */
    setNetwork() {
        this.$scope.checkGraphType();
    }

    initEditMode() {
        super.initEditMode();
        this.addEditorTab('Network', 'public/plugins/gnb/component/modules/display_network/edit/choose_network.html');
        this.editorTabIndex = 1;
    }
}

DisplayNetworkCtrl.templateUrl = 'public/plugins/gnb/component/modules/display_network/display_network.html';

export {DisplayNetworkCtrl as PanelCtrl};
